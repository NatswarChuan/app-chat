import {FontAwesome} from '@expo/vector-icons';
import * as Font from 'expo-font';
import {useEffect,} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {State, updateAccess} from '../redux';

export default function useCachedResources() {
  const {status} = useSelector((state: State) => state.AccessReducer);
  const dispatch = useDispatch();

  // Load any resources or data that we need prior to rendering the app
  useEffect(() => {
    loadResourcesAndDataAsync();
  }, []);

  async function loadResourcesAndDataAsync() {
    try {
      // Load fonts
      await Font.loadAsync({
        ...FontAwesome.font,
        'space-mono': require('../assets/fonts/SpaceMono-Regular.ttf'),
      });
    } catch (e) {
      // We might want to provide this error information to an error reporting service
      console.warn(e);
    } finally {
      dispatch(updateAccess());
    }
  }

  return status;
}
