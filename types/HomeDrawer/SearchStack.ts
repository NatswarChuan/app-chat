import { DrawerNavigationProp } from '@react-navigation/drawer';
import { CompositeNavigationProp } from '@react-navigation/native';
import { NativeStackNavigationProp, NativeStackScreenProps } from '@react-navigation/native-stack';
import { FriendsDrawerParamList } from './FriendsStack';
import { HomeDrawerParamList } from './HomeDrawer';

/**
 * all screen in Search stack
 */
export type SearchDrawerParamList = {
  SearchScreen: undefined;
  SearchResult: undefined;
};

/**
 * changes screen of drawer navigator
 * @param Screen is the screen of Home drawer navigator
 */
export type SearchDrawerScreenProps<
  Screen extends keyof SearchDrawerParamList
> = NativeStackScreenProps<SearchDrawerParamList, Screen>;

/**
 * props of Search stack with React Navigation
 * @param Screen is the screen of Search stack 
 */
export type SearchDrawerScreenNavigationProps<
  Screen extends keyof HomeDrawerParamList
> = CompositeNavigationProp<
  DrawerNavigationProp<HomeDrawerParamList, Screen>,
  NativeStackNavigationProp<FriendsDrawerParamList>
>;