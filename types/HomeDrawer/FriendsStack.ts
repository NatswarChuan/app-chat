import { DrawerNavigationProp } from '@react-navigation/drawer';
import { CompositeNavigationProp } from '@react-navigation/native';
import {
  NativeStackNavigationProp,
  NativeStackScreenProps,
} from '@react-navigation/native-stack';
import { ChatDrawerParamList } from './ChatStack';
import { HomeDrawerParamList } from './HomeDrawer';

/**
 * all screen in Friend stack with React Navigation
 */
export type FriendsDrawerParamList = {
  FriendsList: undefined;
  UserInfo: undefined;
};

/**
 * changes screen of drawer navigator
 * @param Screen is the screen of Home drawer navigator
 */
export type FriendsDrawerScreenNavigationProps<
  Screen extends keyof HomeDrawerParamList
> = CompositeNavigationProp<
  DrawerNavigationProp<HomeDrawerParamList, Screen>,
  NativeStackNavigationProp<ChatDrawerParamList>
>;

/**
 * props of Friend stack with React Navigation
 * @param Screen is the screen of Friend stack 
 */
export type FriendsDrawerScreenProps<
  Screen extends keyof FriendsDrawerParamList
> = NativeStackScreenProps<FriendsDrawerParamList, Screen>;
