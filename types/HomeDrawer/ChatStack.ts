import { NativeStackScreenProps } from '@react-navigation/native-stack';

/**
 * all screen in chat stack with React Navigation
 */
export type ChatDrawerParamList = {
  RoomChat: undefined;
  EditRoomChat: undefined;
  RoomChatInfo: undefined;
};

/**
 * props of chat stack with React Navigation
 * @param Screen is the screen of chat stack 
 */
export type ChatDrawerScreenProps<Screen extends keyof ChatDrawerParamList> =
  NativeStackScreenProps<ChatDrawerParamList, Screen>;
