import { NativeStackScreenProps } from '@react-navigation/native-stack';

/**
 * all screen in Profile stack
 */
export type ProfileDrawerParamList = {
  ProfileScreen: undefined;
  EditProfile: undefined;
  ChangePassword: undefined;
};

/**
 * props of Profile stack
 * @param Screen is the screen of Profile stackx
 */
export type ProfileDrawerScreenProps<
  Screen extends keyof ProfileDrawerParamList
> = NativeStackScreenProps<ProfileDrawerParamList, Screen>;
