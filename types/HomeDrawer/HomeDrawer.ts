import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { NavigatorScreenParams } from '@react-navigation/native';
import { ChatDrawerParamList } from './ChatStack';
import { FriendsDrawerParamList } from './FriendsStack';
import { ProfileDrawerParamList } from './ProfileStack';
import { SearchDrawerParamList } from './SearchStack';

/**
 * all stack in home drawer navigation
 */
export type HomeDrawerParamList = {
  Chats: NavigatorScreenParams<ChatDrawerParamList> | undefined;
  Friends: NavigatorScreenParams<FriendsDrawerParamList> | undefined;
  Profile: NavigatorScreenParams<ProfileDrawerParamList> | undefined;
  Search: NavigatorScreenParams<SearchDrawerParamList> | undefined;
};

/**
 * props of home drawer navigation
 * @param Screen is the stack of home drawer navigation
 */
export type HomeDrawerScreenProps<Screen extends keyof HomeDrawerParamList> =
  NativeStackScreenProps<HomeDrawerParamList, Screen>;
