import { NativeStackScreenProps } from '@react-navigation/native-stack';

export type NewPasswordParams = {
  otp: string;
}

export type ForgotPasswordOTPParams = {
  email: string;
}

/**
 * all screen in login stack with React Navigation
 */
export type LoginStackParamList = {
  Login: undefined;
  Register: undefined;
  RegisterOTP: undefined;
  ForgotPassword: undefined;
  ForgotPasswordOTP: ForgotPasswordOTPParams;
  NewPassword: NewPasswordParams;
};

/**
 * props of login stack with React Navigation
 * @param Screen is the screen of login stack 
 */
export type LoginStackScreenProps<Screen extends keyof LoginStackParamList> =
  NativeStackScreenProps<LoginStackParamList, Screen>;