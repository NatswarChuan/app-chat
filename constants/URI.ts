const IPv4 = '20.214.110.147';
const port = '2909';

const createLink=(api?:string) =>{
  return `http://${IPv4}:${port}/api/${api??''}`;
};

export default createLink;
