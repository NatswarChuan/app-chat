const tintColorLight = '#2f95dc';
const tintColorDark = '#fff';

const colorButtonLight = '#fff';
const colorButtonDark = '#212831';

const headerColorDark = '#212d3a';
const headerColorLight = '#fff';

/**
 * color defined
 */
export default {
  light: {
    text: '#000',
    background: '#fff',
    tint: tintColorLight,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorLight,
    colorButton: colorButtonLight,
    headerLeftIconColor: colorButtonDark,
    headerColor: headerColorLight,
    colorInputDefault: "#C8C8C8",
    colorInputInvalid: "#ED5E5E",
    colorInputFocus: "#37A9F0"
  },
  dark: {
    text: '#fff',
    background: '#212831',
    tint: tintColorDark,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorDark,
    colorButton: colorButtonDark,
    headerLeftIconColor: colorButtonLight,
    headerColor: headerColorDark,
    colorInputDefault: "#485B6F",
    colorInputInvalid: "#F34949",
    colorInputFocus: "#6EB2EF"
  },
};
