import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Platform, StyleSheet } from 'react-native';
import { View, ViewProps } from './Themed/ViewThemed';

export const Container = (props: ViewProps) => {
  const {children} = props;
  return (
    <View style={styles.container} {...props} >
      {children}
      <StatusBar style={Platform.OS === 'ios' ? 'light' : 'auto'} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
