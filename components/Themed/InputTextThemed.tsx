import React, { RefObject } from 'react';
import { StyleSheet, TextInput, ViewStyle } from 'react-native';
import { Text } from './TextThemed';
import { ThemeProps, useThemeColor } from './Themed';
import { View, } from './ViewThemed';

export type TextInputProps = TextInput['props'];

export type InputTextType = ThemeProps & TextInputProps & {
  viewStyle: ViewStyle | ViewStyle[],
  label: string,
  color: string,
  myRef: RefObject<TextInput>
}

/**
 * 
 * @param color is color of label and borderColor 
 * @param label is text of label
 * @param ref is ref of TextInput
 * @param viewStyle is style of view
 * @returns 
 */
const InputTextThemed = (props: InputTextType) => {
  const { color, label, myRef, viewStyle, darkColor, lightColor, ...orderProps } = props;
  const backgroundColor = useThemeColor(
    { light: lightColor, dark: darkColor },
    'background'
  );

  const { style } = orderProps;

  return (
    <View style={viewStyle}>
      <TextInput {...orderProps} ref={myRef} style={[style, { borderColor: color }]} />
      <Text style={[styles.label, { backgroundColor: backgroundColor, color: color }]}>{label}</Text>
    </View>
  );
};

export default InputTextThemed;

const styles = StyleSheet.create({
  // css nut submit
  label: {
    position: 'absolute',
    left: 15,
    top: -10,
    fontSize: 14,
    textAlign: 'center',
    marginLeft: '8%',
  },
});