import { ThemeProps, useThemeColor } from './Themed';
import { Text as DefaultText } from 'react-native';
import React from 'react';

/**
 * props of custom text
 */
export type TextProps = ThemeProps & DefaultText['props'];

/**
 * custom text
 * @param props props of text
 * @returns custom text themed
 */
export function Text(props: TextProps) {
  const {style, lightColor, darkColor, ...otherProps} = props;
  const color = useThemeColor({light: lightColor, dark: darkColor}, 'text');

  return <DefaultText style={[{color}, style]} {...otherProps} />;
}
