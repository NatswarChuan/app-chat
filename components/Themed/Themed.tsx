/**
 * Learn more about Light and Dark modes:
 * https://docs.expo.io/guides/color-schemes/
 */
import Colors from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';

/**
 * get color
 * @param props color light and dark
 * @param colorName name of color defined in the constants Colors.ts file
 * @returns a color get in the constants Colors.ts file
 */
export function useThemeColor(
  props: { light?: string; dark?: string },
  colorName: keyof typeof Colors.light & keyof typeof Colors.dark,
) {
  const theme = useColorScheme();
  const colorFromProps = props[theme];

  if (colorFromProps) {
    return colorFromProps;
  } else {
    return Colors[theme][colorName];
  }
}

export type ThemeProps = {
  lightColor?: string;
  darkColor?: string;
};
