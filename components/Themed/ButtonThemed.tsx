import { ThemeProps, useThemeColor } from './Themed';
import { TouchableOpacity as DefaultButton, } from 'react-native';
import React from 'react';

/**
 * props of custom button
 */
export type ButtonProps = ThemeProps & DefaultButton['props'];

/**
 * custom button
 * @param props button props
 * @returns custom button themed
 */
export function Button(props: ButtonProps) {
  const { style, lightColor, darkColor, ...otherProps } = props;
  const backgroundColor = useThemeColor(
    { light: lightColor, dark: darkColor },
    'colorButton',
  );

  return <DefaultButton style={[style, { backgroundColor }]} {...otherProps} />;
}
