import { View as DefaultView } from 'react-native';
import { ThemeProps, useThemeColor } from './Themed';
import React from 'react';

/**
 * props of custom view
 */
export type ViewProps = ThemeProps & DefaultView['props'];

/**
 * custom view
 * @param props props of view
 * @returns custom view themed
 */
export function View(props: ViewProps) {
  const { style, lightColor, darkColor, ...otherProps } = props;
  const backgroundColor = useThemeColor(
    { light: lightColor, dark: darkColor },
    'background'
  );

  return <DefaultView style={[{ backgroundColor }, style]} {...otherProps} />;
}
