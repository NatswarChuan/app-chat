import { ThemeProps, useThemeColor } from './Themed';
import { FontAwesome  as DefaultIcon } from '@expo/vector-icons';
import { StyleProp, TextStyle } from 'react-native';
import { GestureResponderEvent } from 'react-native';
import React from 'react';

/**
 * custom icon
 * @param props props of icons
 * @returns custom icon themed
 */
export function HeaderLeftIcon(
  props: {
    name: React.ComponentProps<typeof DefaultIcon>['name'];
    myStyle?: StyleProp<TextStyle>;
    props: {
      tintColor?: string | undefined;
      pressColor?: string | undefined;
      pressOpacity?: number | undefined;
      labelVisible?: boolean | undefined;
    };
    onPress?: (event: GestureResponderEvent) => void;
  } & ThemeProps
) {
  const { myStyle, lightColor, darkColor, ...otherProps } = props;
  const headerLeftIconColor = useThemeColor(
    { light: lightColor, dark: darkColor },
    'headerLeftIconColor'
  );

  return (
    <DefaultIcon
      size={20}
      style={[{ marginBottom: -3, color: headerLeftIconColor }, myStyle]}
      {...otherProps}
    />
  );
}
