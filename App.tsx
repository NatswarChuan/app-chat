import {Provider} from 'react-redux';
import {store} from './redux/store/index';
import Screens from './screens/Screens';
import React from 'react';

export default function App() {
  return (
    <Provider store={store}>
      <Screens />
    </Provider>
  );
}
