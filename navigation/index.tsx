/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
} from '@react-navigation/native';
import React, { useEffect } from 'react';
import { ColorSchemeName } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { checkLogin, State } from '../redux';
import { UserModel } from '../redux/models/dto/UserModel';
import HomeDrawerScreen from './HomeDrawerScreen';
import LoginStackScreen from './LoginStackScreen';

export default function Navigation({ colorScheme, }: { colorScheme: ColorSchemeName; }) {
  const { user }: { user: UserModel } = useSelector((state: State) => state.UserReducer);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(checkLogin());
  }, []);

  return (
    <NavigationContainer theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
      {user && user.id ? <HomeDrawerScreen /> : <LoginStackScreen />}
    </NavigationContainer>
  );
}
