import {createNativeStackNavigator} from '@react-navigation/native-stack';
import { ProfileDrawerParamList} from '../types';
import {
  ChangePassword,
  EditProfile,
  Profile,
} from '../screens';
import React from 'react';

const Stack = createNativeStackNavigator<ProfileDrawerParamList>();

export default function ProfileStackScreen() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="ProfileScreen" component={Profile} />
      <Stack.Screen name="EditProfile" component={EditProfile} />
      <Stack.Screen name="ChangePassword" component={ChangePassword} />
    </Stack.Navigator>
  );
}
