import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import { SearchDrawerParamList} from '../types';
import {
  Search,
  SearchResult,
} from '../screens';

const Stack = createNativeStackNavigator<SearchDrawerParamList>();

export default function SearchStackScreen() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="SearchScreen" component={Search} />
      <Stack.Screen name="SearchResult" component={SearchResult} />
    </Stack.Navigator>
  );
}
