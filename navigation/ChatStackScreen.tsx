import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {ChatDrawerParamList} from '../types';
import {
  EditRoomChat,
  RoomChat,
  RoomChatInfo,
} from '../screens';
import React from 'react';

const Stack = createNativeStackNavigator<ChatDrawerParamList>();

export default function ChatStackScreen() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="RoomChat" component={RoomChat} />
      <Stack.Screen name="EditRoomChat" component={EditRoomChat} />
      <Stack.Screen name="RoomChatInfo" component={RoomChatInfo} />
    </Stack.Navigator>
  );
}
