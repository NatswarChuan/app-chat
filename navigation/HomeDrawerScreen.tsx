import { createDrawerNavigator } from '@react-navigation/drawer';
import { HeaderLeftIcon } from '../components/Themed/IconThemed';

import React from 'react';
import { HomeDrawerParamList } from '../types';
import ChatStackScreen from './ChatStackScreen';
import FriendsStackScreen from './FriendsStackScreen';
import ProfileStackScreen from './ProfileStackScreen';
import SearchStackScreen from './SearchStackScreen';
import { RouteProp } from '@react-navigation/native';
import { useThemeColor } from '../components/Themed/Themed';
// import { HeaderLeftIcon } from './../components/Themed/IconThemed';

const Drawer = createDrawerNavigator<HomeDrawerParamList>();

export default function HomeDrawerScreen() {
  return (
    <Drawer.Navigator
      initialRouteName="Chats"
      backBehavior="firstRoute"
      useLegacyImplementation={true}
      screenOptions={drawerScreenOption}
    >
      <Drawer.Screen name="Chats" component={ChatStackScreen} />
      <Drawer.Screen name="Friends" component={FriendsStackScreen} />
      <Drawer.Screen name="Profile" component={ProfileStackScreen} />
      <Drawer.Screen name="Search" component={SearchStackScreen} />
    </Drawer.Navigator>
  );
}
/* eslint-disable  @typescript-eslint/no-explicit-any */
const drawerScreenOption = (props: {
  route: RouteProp<HomeDrawerParamList, keyof HomeDrawerParamList>;
  navigation: any;
}) => {
  const headerColor = useThemeColor(
    { light: undefined, dark: undefined },
    'headerColor'
  );

  return {
    headerLeft: (prop: {
      tintColor?: string | undefined;
      pressColor?: string | undefined;
      pressOpacity?: number | undefined;
      labelVisible?: boolean | undefined;
    }) => (
      <HeaderLeftIcon
        props={{ ...prop }}
        name="bars"
        myStyle={{ padding: 20 }}
        // myStyle={{ padding: "11px" }}
        onPress={() => {
          props.navigation.toggleDrawer();
        }}
      />
    ),
    drawerStyle: {
      backgroundColor: headerColor,
    },
    headerStyle: {
      backgroundColor: headerColor,
    },
  };
};