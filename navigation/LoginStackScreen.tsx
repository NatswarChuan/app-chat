import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {LoginStackParamList} from '../types';
import React from 'react';
import {
  ForgotPassword,
  ForgotPasswordOTP,
  Login,
  NewPassword,
  Register,
  RegisterOTP,
} from '../screens';

const Stack = createNativeStackNavigator<LoginStackParamList>();

export default function LoginStackScreen() {
  return (
    <Stack.Navigator initialRouteName="Login" screenOptions={{headerShown: false}}>
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
      <Stack.Screen name="ForgotPasswordOTP" component={ForgotPasswordOTP} />
      <Stack.Screen name="NewPassword" component={NewPassword} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="RegisterOTP" component={RegisterOTP} />
    </Stack.Navigator>
  );
}
