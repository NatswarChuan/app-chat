import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { FriendsDrawerParamList } from '../types';
import { FriendsList, UserInfo } from '../screens';
import React from 'react';

const Stack = createNativeStackNavigator<FriendsDrawerParamList>();

export default function FriendsStackScreen() {
  return (
    <Stack.Navigator
      screenOptions={{ headerShown: false }}
      initialRouteName="FriendsList"
    >
      <Stack.Screen name="FriendsList" component={FriendsList} />
      <Stack.Screen name="UserInfo" component={UserInfo} />
    </Stack.Navigator>
  );
}
