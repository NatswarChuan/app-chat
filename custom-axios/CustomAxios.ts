import axios, { AxiosRequestConfig } from 'axios';
import createLink from '../constants/URI';

export default class CustomAxios {
  /**
   * send request to server
   * @param link link api
   * @param config custom config
   * @returns response
   */
  public static async get<T>(link?: string, config?: AxiosRequestConfig) {
    const _config: typeof config = {
      ...config,
    };

    return await axios.get<T>(createLink(link), _config);
  }

  /**
   * send request to server method post
   * @param link link api
   * @param config custom config
   * @returns response
   */ 
  /* eslint-disable  @typescript-eslint/no-explicit-any */
  public static async post<T>(link?: string,data?: any, config?: AxiosRequestConfig) { 
    const _config: typeof config = {
      ...config,
    };

    return await axios.post<T>(createLink(link),data, _config);
  }
}
