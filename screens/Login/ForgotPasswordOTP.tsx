import React, { useEffect, useState } from 'react';
import { LoginStackScreenProps, NewPasswordParams } from '../../types';
import { Container } from '../../components/Container';
import { Button, StyleSheet, TextInput } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { forgotPasswordOTP, OTPForgotPasswordRequest, State } from '../../redux';

/**
 * screen Forgot Password OTP
 * @param props get navigation and route to changes screen to another in chat stack
 * @returns JSX.Element
 */
export const ForgotPasswordOTP = (props: LoginStackScreenProps<'ForgotPasswordOTP'>) => {
  const { navigation } = props;
  // const request = route.params;
  const [OTP, setOTP] = useState<string>('');
  const { updateDate } = useSelector((state: State) => state.UserReducer);
  const dispatch = useDispatch();
  const sendOTP = () => {
    const request: OTPForgotPasswordRequest = { otp: OTP };
    dispatch(forgotPasswordOTP(request));
  };
  useEffect(() => {
    if (updateDate) {
      const params: NewPasswordParams = { otp: OTP };
      navigation.push('NewPassword', params);
    }
  }, [updateDate]);
  return (
    <Container>
      <TextInput value={OTP} style={styles.input} onChangeText={setOTP} />
      <Button
        title="Press me"
        color="#f194ff"
        onPress={sendOTP}
      />
    </Container>
  );
};
const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});
