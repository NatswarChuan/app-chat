import React, { useEffect, useState } from 'react';
import { LoginStackScreenProps, ForgotPasswordOTPParams } from '../../types';
import { Container } from '../../components/Container';
import { useDispatch, useSelector } from 'react-redux';
import { Button, StyleSheet, TextInput } from 'react-native';
import { forgotPassword, State, UserForgotPasswordRequest } from '../../redux';
import { dicStatus } from '../../constants/dicStatus';

/**
 * screen Forgot Password
 * @param props get navigation and route to changes screen to another in chat stack
 * @returns JSX.Element
 */
export const ForgotPassword = (props: LoginStackScreenProps<'ForgotPassword'>) => {
  const { navigation } = props;
  const [email, setEmail] = useState<string>('');
  const { forgotPasswordStatus } = useSelector((state: State) => state.UserReducer);
  const dispatch = useDispatch();
  const sendEmail = () => {
    const request: UserForgotPasswordRequest = {
      email: email,
    };
    dispatch(forgotPassword(request));
  };

  useEffect(() => {
    if (dicStatus[forgotPasswordStatus] == 'OK') {
      const params: ForgotPasswordOTPParams = {
        email: email,
      };
      navigation.push('ForgotPasswordOTP', params);
    }
  }, [forgotPasswordStatus]);

  //#region 
  return (
    <Container>
      <TextInput value={email} style={styles.input} onChangeText={setEmail} />
      <Button
        title="Press me"
        color="#f194ff"
        onPress={sendEmail}
      />
    </Container>
  );
};
//#endregion

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});
