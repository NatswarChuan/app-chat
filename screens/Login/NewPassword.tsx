import React from 'react';
import { Text } from '../../components/Themed/TextThemed';
import { Button } from '../../components/Themed/ButtonThemed';
import {LoginStackScreenProps} from '../../types';
import { Container } from '../../components/Container';

/**
 * screen New Password
 * @param props get navigation and route to changes screen to another in chat stack
 * @returns JSX.Element
 */
export const NewPassword = (props: LoginStackScreenProps<'NewPassword'>) => {
  const { navigation } = props;

  return (
    <Container>
      <Button onPress={() => navigation.replace('Login')}>
        <Text style={{fontSize: 24}}>Go Home</Text>
      </Button>
    </Container>
  );
};
