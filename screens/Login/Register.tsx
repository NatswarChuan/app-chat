import React, { useEffect, useRef, useState } from 'react';
import { Container } from '../../components/Container';
import { Button } from '../../components/Themed/ButtonThemed';
import { Text } from '../../components/Themed/TextThemed';
import { LoginStackScreenProps } from '../../types';
// import { useDispatch, } from 'react-redux';
import { KeyboardTypeOptions, StyleSheet, TextInput, View } from 'react-native';
import InputTextThemed from '../../components/Themed/InputTextThemed';
import { RegexEmail, RegexPassword, RegexPhone } from '../../constants/Regex';
import { useThemeColor } from '../../components/Themed/Themed';

type ColorInputRegister = {
  name: string;
  email: string;
  password: string;
  confirmPassword: string;
  phone: string;
}

type ValueInputRegister = ColorInputRegister

type InputField = {
  id: number
  color: string
  ref: React.RefObject<TextInput>
  value: string
  name: string
  autoFocus?: boolean
  keyboardType?: KeyboardTypeOptions
  refNext?: React.RefObject<TextInput>
  label: string
  validField?: (value: string) => boolean
}

/**
 * screen Register
 * @param props get navigation and route to changes screen to another in chat stack
 * @returns JSX.Element
 */
export const Register = (props: LoginStackScreenProps<'Register'>) => {
  const { navigation } = props;

  const [colorInputDefault, setColorInputDefault] = useState<string>(useThemeColor(
    { light: undefined, dark: undefined },
    'colorInputDefault'
  ))
  const [colorInputFocus, setColorInputFocus] = useState<string>(useThemeColor(
    { light: undefined, dark: undefined },
    'colorInputFocus'
  ))
  const [colorInputInvalid, setColorInputInvalid] = useState<string>(useThemeColor(
    { light: undefined, dark: undefined },
    'colorInputInvalid'
  ))

  const [valueInput, setValueInput] = useState<ValueInputRegister>({
    name: "",
    email: "",
    password: "",
    confirmPassword: "",
    phone: ""
  })

  const [colorInput, setColorInput] = useState<ColorInputRegister>({
    name: colorInputDefault || "red",
    email: colorInputDefault || "red",
    password: colorInputDefault || "red",
    confirmPassword: colorInputDefault || "red",
    phone: colorInputDefault || "red",
  })

  const email = useRef<TextInput>(null)
  const password = useRef<TextInput>(null)
  const name = useRef<TextInput>(null)
  const phone = useRef<TextInput>(null)
  const confirmPassword = useRef<TextInput>(null)

  const arrayInputField: InputField[] = [
    {
      id: 0,
      color: colorInput.name,
      ref: name,
      value: valueInput.name,
      name: "name",
      autoFocus: true,
      refNext: phone,
      label: "Name"
    },
    {
      id: 1,
      color: colorInput.phone,
      ref: phone,
      value: valueInput.phone,
      name: "phone",
      refNext: email,
      label: "Phone",
      validField: (value: string) => {
        return RegexPhone.test(value)
      }
    },
    {
      id: 2,
      color: colorInput.email,
      ref: email,
      value: valueInput.email,
      name: "email",
      refNext: password,
      label: "Email",
      validField: (value: string) => {
        return RegexEmail.test(value)
      }
    },
    {
      id: 3,
      color: colorInput.password,
      ref: password,
      value: valueInput.password,
      name: "password",
      refNext: confirmPassword,
      label: "Password",
      validField: (value: string) => {
        return RegexPassword.test(value)
      }
    },
    {
      id: 4,
      color: colorInput.confirmPassword,
      ref: confirmPassword,
      value: valueInput.confirmPassword,
      name: "confirmPassword",
      label: "ConfirmPassword",
      validField: (value: string) => {
        return RegexPassword.test(value) && valueInput.password === valueInput.confirmPassword
      }
    },
  ]

  return (
    <Container>
      <View style={{ width: '100%' }}>
        <Text style={{ textAlign: 'center', fontSize: 20, marginBottom: 12 }}>Register</Text>
        <Text style={{ textAlign: 'center', color: '#7D8B99' }}>Please enter your email and password</Text>

        {arrayInputField.map((valueArray, index) => {
          return (
            <InputTextThemed
              key={valueArray.id}
              value={valueArray.value}
              onChangeText={value => {
                setValueInput((prev) => {
                  return {
                    ...prev,
                    [valueArray.name]: value
                  }
                });
              }}
              color={valueArray.color}
              myRef={valueArray.ref}
              autoFocus={valueArray.autoFocus}
              keyboardType={valueArray.keyboardType}
              viewStyle={[styles.viewInputStyle, { marginTop: index === 0 ? 23 : 60 }]}
              onFocus={() => {
                setColorInput(prev => {
                  return {
                    ...prev,
                    [valueArray.name]: colorInputFocus || "red"
                  }
                });
              }}
              onBlur={() => {
                colorInputDefault && colorInputInvalid && setColorInput(prev => {
                  return {
                    ...prev,
                    [valueArray.name]: valueArray.validField ? (valueArray.validField(valueArray.value) ? colorInputDefault : colorInputInvalid) : colorInputDefault
                  }
                });
              }}
              onSubmitEditing={() => {
                valueArray.refNext && valueArray.refNext.current?.focus();
              }}
              style={[styles.inputStyle, { borderColor: valueArray.color }]}
              label={`${valueArray.label}`}
            />
          )
        })}

        <View style={{ flexDirection: 'row', marginHorizontal: '10%', justifyContent: 'space-between', marginTop: 53 }}>
          <Button onPress={() => navigation.push('ForgotPassword')}>
            <Text style={{ fontSize: 14, }}>Forgot password?</Text>
          </Button>
          <Button onPress={() => navigation.replace("Login")}>
            <Text style={{ fontSize: 14, }}>Login</Text>
          </Button>
        </View>
      </View>
    </Container>
  );
};

const styles = StyleSheet.create({
  label: {
    position: 'absolute',
    left: 15,
    top: -10,
    fontSize: 14,
    textAlign: 'center',
    marginLeft: '8%',
  },
  inputStyle: {
    width: '80%',
    height: 39,
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    color: '#fff',
  },
  viewInputStyle: {
    flex: 1,
    width: '100%',
    alignItems: 'center'
  }
});