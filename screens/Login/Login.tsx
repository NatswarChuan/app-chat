import React, { useRef, useState, } from 'react';
import { LoginStackScreenProps } from '../../types';
// import { useDispatch, } from 'react-redux';
import { Button } from '../../components/Themed/ButtonThemed';
import { Text } from '../../components/Themed/TextThemed';
import { Container } from '../../components/Container';
import { StyleSheet, TextInput, View } from 'react-native';
import InputTextThemed from '../../components/Themed/InputTextThemed';
import { RegexEmail, RegexPassword } from '../../constants/Regex';

//#region 
/**
 * screen Login
 * @param props get navigation and route to changes screen to another in chat stack
 * @returns JSX.Element
 */
export const Login = (props: LoginStackScreenProps<'Login'>) => {
  const { navigation } = props;
  // const dispatch = useDispatch();

  const [emailValue, setEmailValue] = useState<string>('');
  const [passwordValue, setPasswordValue] = useState<string>('');
  const email = useRef<TextInput>(null);
  const password = useRef<TextInput>(null);
  const [colorEmail, setColorEmail] = useState('#485B6F');
  const [colorPassword, setColorPassword] = useState('#485B6F');

  const validEmail = (emailText: string) => {
    return RegexEmail.test(emailText);
  };

  // cau lenh check valid password
  const validPassword = (passwordText: string) => {
    return RegexPassword.test(passwordText);
  };

  //#region 
  return (
    <Container>
      <View style={{ width: '100%' }}>
        <Text style={{ textAlign: 'center', fontSize: 20, marginBottom: 12 }}>Login</Text>
        <Text style={{ textAlign: 'center', color: '#7D8B99' }}>Please enter your email and password</Text>

        <InputTextThemed
          value={emailValue}
          onChangeText={value => {
            setEmailValue(value);
          }}
          color={colorEmail}
          myRef={email}
          autoFocus
          keyboardType='email-address'
          viewStyle={[styles.viewInputStyle, { marginTop: 23 }]}
          onFocus={() => {
            setColorEmail('#6EB2EF');
          }}
          onBlur={() => {
            setColorEmail(validEmail(emailValue) ? '#485B6F' : 'red');
          }}
          onSubmitEditing={() => {
            password.current?.focus();
          }}
          style={[styles.inputStyle, { borderColor: colorEmail }]}
          label="Email"
        />

        <InputTextThemed
          value={passwordValue}
          onChangeText={value => {
            setPasswordValue(value);
          }}
          color={colorPassword}
          myRef={password}
          secureTextEntry
          viewStyle={[styles.viewInputStyle, { marginTop: 60 }]}
          onFocus={() => {
            setColorPassword('#6EB2EF');
          }}
          onBlur={() => {
            setColorPassword(validPassword(passwordValue) ? '#485B6F' : 'red');
          }}
          /* eslint-disable @typescript-eslint/no-empty-function */
          onSubmitEditing={() => {

          }}
          style={[styles.inputStyle, { borderColor: colorPassword }]}
          label="Password"
        />

        <View style={{ flexDirection: 'row', marginHorizontal: '10%', justifyContent: 'space-between', marginTop: 53 }}>
          <Button onPress={() => navigation.push('ForgotPassword')}>
            <Text style={{ fontSize: 14, }}>Forgot password?</Text>
          </Button>
          <Button onPress={() => navigation.push('Register')}>
            <Text style={{ fontSize: 14, }}>Register</Text>
          </Button>
        </View>
      </View>
    </Container>
  );
};
//#endregion

//#region 
const styles = StyleSheet.create({
  label: {
    position: 'absolute',
    left: 15,
    top: -10,
    fontSize: 14,
    textAlign: 'center',
    marginLeft: '8%',
  },
  inputStyle: {
    width: '80%',
    height: 39,
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    color: '#fff',
  },
  viewInputStyle: {
    flex: 1,
    width: '100%',
    alignItems: 'center'
  }
});
//#endregion