import React from 'react';
import { Container } from '../../components/Container';
import { Button } from '../../components/Themed/ButtonThemed';
import { Text } from '../../components/Themed/TextThemed';
import { LoginStackScreenProps, NewPasswordParams } from '../../types';

/**
 * screen Register OTP
 * @param props get navigation and route to changes screen to another in chat stack
 * @returns JSX.Element
 */
export const RegisterOTP = (props: LoginStackScreenProps<'RegisterOTP'>) => {
  const { navigation } = props;

  return (
    <Container>
      <Button onPress={() => {
        const params: NewPasswordParams = { otp: '' };
        navigation.push('NewPassword', params);
      }}>
        <Text style={{ fontSize: 24 }}>Go to New Password</Text>
      </Button>
    </Container>
  );
};