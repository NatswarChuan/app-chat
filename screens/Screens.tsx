import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import useCachedResources from '../hooks/useCachedResources';
import useColorScheme from '../hooks/useColorScheme';
import Navigation from '../navigation';
import { StatusBar } from 'expo-status-bar';
import { Lauding } from './Landing/Landing';
import { View } from '../components/Themed/ViewThemed';
import { Text } from '../components/Themed/TextThemed';
import { StyleSheet } from 'react-native';
import { dicStatus } from '../constants/dicStatus';


/**
 * send request to server if get response success go to navigation if get response error go to error screen
 * @returns JSX.Element
 */
export default function Screens() {
  const status: number = useCachedResources();
  const colorScheme = useColorScheme();

  return (
    <>
      {status === 200 ? (
        <SafeAreaProvider>
          <Navigation colorScheme={colorScheme} />
          <StatusBar />
        </SafeAreaProvider>
      ) : status === 0 ? (
        <Lauding />
      ) : (
        <View style={styles.container}>
          <Text style={styles.title}>{dicStatus[status]}</Text>
        </View>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});
