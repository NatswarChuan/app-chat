import React from 'react';
import { Button } from '../../components/Themed/ButtonThemed';
import { Text } from '../../components/Themed/TextThemed';
import { ProfileDrawerScreenProps } from '../../types';
import { Container } from './../../components/Container';

/**
 * screen Change Password
 * @param props get navigation and route to changes screen to another in chat stack
 * @returns JSX.Element
 */
export const ChangePassword = (props: ProfileDrawerScreenProps<'ChangePassword'>) => {
  const { navigation } = props;

  return (
    <Container>
      <Button onPress={() => navigation.replace('ProfileScreen')}>
        <Text style={{ fontSize: 24, paddingBottom: 24 }}>
          Go To Profile Screen
        </Text>
      </Button>
    </Container>
  );
};
