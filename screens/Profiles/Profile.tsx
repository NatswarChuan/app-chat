import React from 'react';
import { Button } from '../../components/Themed/ButtonThemed';
import { Text } from '../../components/Themed/TextThemed';
import { ProfileDrawerScreenProps } from '../../types';
import { Container } from './../../components/Container';

/**
 * screen Profile
 * @param props get navigation and route to changes screen to another in chat stack
 * @returns JSX.Element
 */
export const Profile = (props: ProfileDrawerScreenProps<'ProfileScreen'>) => {
  const { navigation } = props;

  return (
    <Container>
      <Button onPress={() => navigation.push('EditProfile')}>
        <Text style={{ fontSize: 24, paddingBottom: 24 }}>
          Go To Edit Profile
        </Text>
      </Button>
      <Button onPress={() => navigation.push('ChangePassword')}>
        <Text style={{ fontSize: 24, paddingBottom: 24 }}>
          Go To Change Password
        </Text>
      </Button>
    </Container>
  );
};
