import React from 'react';
import { Button } from '../../components/Themed/ButtonThemed';
import { Text } from '../../components/Themed/TextThemed';
import { ChatDrawerScreenProps } from '../../types';
import { Container } from './../../components/Container';

/**
 * screen romm chat info
 * @param props get navigation and route to changes screen to another in chat stack
 * @returns JSX.Element
 */
export const RoomChatInfo = (props: ChatDrawerScreenProps<'RoomChatInfo'>) => {
  const { navigation } = props;

  return (
    <Container>
      <Button onPress={() => navigation.replace('RoomChat')}>
        <Text style={{ fontSize: 24, paddingBottom: 24 }}>Go Room Chat</Text>
      </Button>
      <Button onPress={() => navigation.push('EditRoomChat')}>
        <Text style={{ fontSize: 24 }}>Go Edit Room Chat</Text>
      </Button>
    </Container>
  );
};
