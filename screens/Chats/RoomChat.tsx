import React from 'react';
import { ChatDrawerScreenProps } from '../../types';
import { Text } from '../../components/Themed/TextThemed';
import { Container } from '../../components/Container';
import { useDispatch } from 'react-redux';
import {
  FlatList,
  StyleSheet,
} from 'react-native';
import { View } from '../../components/Themed/ViewThemed';
import Swipeable from 'react-native-gesture-handler/Swipeable';

interface data {
  id: string;
  title: string;
}

const DATA: data[] = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'First Item',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Second Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Third Item',
  },
];

/**
 * screen romm chat
 * @param props get navigation and route to changes screen to another in chat stack
 * @returns JSX.Element
 */
export const RoomChat = (props: ChatDrawerScreenProps<'RoomChat'>) => {
  const { navigation } = props;
  // const { user }: { user: UserModel } = useSelector(
  //   (state: State) => state.UserReducer
  // );
  // const dispatch = useDispatch();

  const RightAction = () => {
    return <View>
      <Text>12312</Text>
    </View>;
  };

  const renderItem = ({ item }: { item: data }) => (
    <Swipeable
      containerStyle={{
        backgroundColor: 'red',
      }}
      renderRightActions={RightAction}
    >
      <Text style={styles.title}>{item.title}</Text>
    </Swipeable>
  );

  return (
    <Container>
      <FlatList
        style={{ width: '100%', }}
        data={DATA}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  logo: {
    width: 66,
    height: 58,
  },
  item: {
    backgroundColor: '#f9c2ff',
    flex: 1,
  },
  title: {
    fontSize: 32,
  },
});
