import React from 'react';
import { Button } from '../../components/Themed/ButtonThemed';
import { Text } from '../../components/Themed/TextThemed';
import { Container } from '../../components/Container';
import { ChatDrawerScreenProps } from '../../types';

/**
 * screen edit romm chat
 * @param props get navigation and route to changes screen to another in chat stack
 * @returns JSX.Element
 */
export const EditRoomChat = (props: ChatDrawerScreenProps<'EditRoomChat'>):JSX.Element => {
  const { navigation } = props;

  return (
    <Container>
      <Button onPress={() => navigation.replace('RoomChat')}>
        <Text style={{ fontSize: 24 }}>Go Room Chat</Text>
      </Button>
    </Container>
  );
};
