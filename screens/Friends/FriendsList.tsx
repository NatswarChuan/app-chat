import React from 'react';
import { Button } from '../../components/Themed/ButtonThemed';
import { Text } from '../../components/Themed/TextThemed';
import { FriendsDrawerScreenProps } from '../../types';
import { Container } from './../../components/Container';

/**
 * screen Friends List
 * @param props get navigation and route to changes screen to another in chat stack
 * @returns JSX.Element
 */
export const FriendsList = (props: FriendsDrawerScreenProps<'FriendsList'>) => {
  const { navigation } = props;

  return (
    <Container>
      <Button onPress={() => navigation.push('UserInfo')}>
        <Text style={{ fontSize: 24, paddingBottom: 24 }}>Go To User Info</Text>
      </Button>
    </Container>
  );
};
