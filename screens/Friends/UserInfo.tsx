import React from 'react';
import { Button } from '../../components/Themed/ButtonThemed';
import { Text } from '../../components/Themed/TextThemed';
import {
  FriendsDrawerScreenNavigationProps,
  FriendsDrawerScreenProps,
} from '../../types';
import { Container } from './../../components/Container';
import { useNavigation } from '@react-navigation/native';

/**
 * screen User Info
 * @param props get navigation and route to changes screen to another in chat stack
 * @returns JSX.Element
 */
export const UserInfo = (props: FriendsDrawerScreenProps<'UserInfo'>) => {
  const { navigation } = props;
  const navigationDrawer = useNavigation<FriendsDrawerScreenNavigationProps<'Friends'>>();

  return (
    <Container>
      <Button onPress={() => { navigationDrawer.navigate('RoomChat'); }}>
        <Text style={{ fontSize: 24, paddingBottom: 24 }}>Go To Room Chat</Text>
      </Button>
      <Button onPress={() => { navigation.replace('FriendsList'); }}>
        <Text style={{ fontSize: 24, paddingBottom: 24 }}>Go To Friends List</Text>
      </Button>
    </Container>
  );
};
