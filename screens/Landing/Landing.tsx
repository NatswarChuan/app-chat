import React, { useEffect } from 'react';
import { StyleSheet } from 'react-native';
import { View } from '../../components/Themed/ViewThemed';
import { Text } from '../../components/Themed/TextThemed';
import { useDispatch, } from 'react-redux';
import { checkLogin, } from '../../redux';

export const Lauding = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(checkLogin());
  }, []);

  return (
    <View style={styles.container}>
      <View style={{ flexDirection: 'column' }}>
        <Text style={{ fontWeight: 'bold', fontSize: 30 }}>Welcome</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
