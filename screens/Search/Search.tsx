import React from 'react';
import { Button } from '../../components/Themed/ButtonThemed';
import { Text } from '../../components/Themed/TextThemed';
import { SearchDrawerScreenProps } from '../../types';
import { Container } from './../../components/Container';

export const Search = ({
  navigation,
}: SearchDrawerScreenProps<'SearchScreen'>) => {
  return (
    <Container>
      <Button onPress={() => navigation.push('SearchResult')}>
        <Text style={{ fontSize: 24, paddingBottom: 24 }}>
          Go To Search Result
        </Text>
      </Button>
    </Container>
  );
};
