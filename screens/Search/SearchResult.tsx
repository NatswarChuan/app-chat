import React from 'react';
import { Button } from '../../components/Themed/ButtonThemed';
import { Text } from '../../components/Themed/TextThemed';
import { SearchDrawerScreenProps,SearchDrawerScreenNavigationProps } from '../../types';
import { Container } from './../../components/Container';
import { useNavigation } from '@react-navigation/native';

export const SearchResult = ({
  navigation,
}: SearchDrawerScreenProps<'SearchResult'>) => {
  const navigationDrawer =
    useNavigation<SearchDrawerScreenNavigationProps<'Friends'>>();

  return (
    <Container>
      <Button onPress={() => navigation.replace('SearchScreen')}>
        <Text style={{ fontSize: 24, paddingBottom: 24 }}>Go To Search Screen</Text>
      </Button>
      <Button onPress={() => navigationDrawer.navigate('UserInfo')}>
        <Text style={{ fontSize: 24, paddingBottom: 24 }}>Go To User Info</Text>
      </Button>
    </Container>
  );
};
