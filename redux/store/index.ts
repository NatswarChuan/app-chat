import rootReducer from '../reducers';
import thunk from 'redux-thunk';
import {applyMiddleware, createStore} from '@reduxjs/toolkit';

export const store = createStore(
  rootReducer,
  applyMiddleware(thunk),
);
