import {Dispatch} from 'redux';
import {AccessActionType} from '../actions-type';
import { Access } from '../models';
/**
 * type of update access dispatch
 */
export interface UpdateAccess {
  readonly type: AccessActionType.UPDATE_ACCESS_INFO;
  payload?: number;
}

/**
 * type of access actions
 */
export type AccessActions = UpdateAccess;

/**
 * update access action
 */
export const updateAccess = () => async (dispatch: Dispatch<AccessActions>) => {
  dispatch({
    type: AccessActionType.UPDATE_ACCESS_INFO,
    payload: await Access.updateAccess(),
  });
};
