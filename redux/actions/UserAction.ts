import { Dispatch } from 'redux';
import { UserActionType } from '../actions-type/UserActionType';
import { UserModel, } from '../models/dto/UserModel';
// import { UserLoginResponse,UserLoginRequest } from '../models';
import {User} from '../models/dto';
import {  OTPForgotPasswordRequest, UserForgotPasswordRequest, UserLoginRequest } from '../models';
export interface CheckLogin {
  readonly type: UserActionType.CHECK_LOGIN;
  payload?: UserModel;
}

export interface Login {
  readonly type: UserActionType.LOGIN;
  payload?: UserModel;
}

export interface Logout {
  readonly type: UserActionType.LOGOUT;
  payload?: UserModel;
}

export interface ForgotPassword {
  readonly type: UserActionType.FORGOT_PASSWORD;
  payload?: number;
}

export interface OTPForgotPassword {
  readonly type: UserActionType.OTP_FORGOT_PASSWORD;
  payload?: string;
}


/**
 * type of access actions
 */
export type UserActions = CheckLogin | Login |Logout |ForgotPassword|OTPForgotPassword;

/**
 * check Login action
 */
export const checkLogin = () => async (dispatch: Dispatch<UserActions>) => {
  dispatch({
    type: UserActionType.CHECK_LOGIN,
    payload: await User.checkLoginFunc(),
  });
};

export const login = (request:UserLoginRequest) => async (dispatch: Dispatch<UserActions>) => {
  dispatch({
    type: UserActionType.LOGIN,
    payload: await User.loginFunc(request),
  });
};


export const logout = () => async (dispatch: Dispatch<UserActions>) => {
  dispatch({
    type: UserActionType.LOGIN,
    payload: await User.logoutFunc(),
  });
};


export const forgotPassword = (request: UserForgotPasswordRequest) => async (dispatch: Dispatch<UserActions>) => {
  dispatch({
    type: UserActionType.FORGOT_PASSWORD,
    payload: await User.forgotPasswordFunc(request),
  });
};


export const forgotPasswordOTP = (request: OTPForgotPasswordRequest) => async (dispatch: Dispatch<UserActions>) => {
  dispatch({
    type: UserActionType.OTP_FORGOT_PASSWORD,
    payload: await User.forgotPasswordOTPFunc(request),
  });
};
