import { UserActions } from '../actions';
import { UserActionType } from '../actions-type/UserActionType';
import { UserModel, UserState } from '../models/dto/UserModel';

const initialState: UserState = {
  user: {} as UserModel,
  forgotPasswordStatus: 0,
  updateDate: '',
};

const UserReducer = (state: UserState = initialState, action: UserActions,) => {
  switch (action.type) {
  case UserActionType.CHECK_LOGIN:
    return {
      ...state,
      user: action.payload,
    };
  case UserActionType.LOGIN:
    return {
      ...state,
      user: action.payload,
    };
  case UserActionType.LOGOUT:
    return {
      ...state,
      user: action.payload,
    };
  case UserActionType.FORGOT_PASSWORD:
    return {
      ...state,
      forgotPasswordStatus: action.payload,
    };
  case UserActionType.OTP_FORGOT_PASSWORD:
    return {
      ...state,
      updateDate: action.payload,
    };
  default:
    return state;
  }
};

export default UserReducer;
