import { AccessActionType } from '../actions-type';
import { AccessActions } from '../actions/AccessAction';
import { AccessState } from '../models';

const initialState: AccessState = {
  status: 0,
};

const AccessReducer = (state: AccessState = initialState, action: AccessActions,) => {
  switch (action.type) {
  case AccessActionType.UPDATE_ACCESS_INFO:
    return {
      ...state,
      status: action.payload,
    };
  default:
    return state;
  }
};

export default AccessReducer;
