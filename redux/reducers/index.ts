import {combineReducers} from '@reduxjs/toolkit';
import AccessReducer from './AccessReducer';
import UserReducer from './UserReducer';

const rootReducer = combineReducers({
  AccessReducer,
  UserReducer,
});

export default rootReducer;

export type State = ReturnType<typeof rootReducer>
