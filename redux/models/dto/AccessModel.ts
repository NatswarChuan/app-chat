import { AxiosError } from 'axios';
import CustomAxios from '../../../custom-axios/CustomAxios';

export interface AccessState {
  status: number;
  error?: string;
}

/**
 * actions of access
 */
export class Access {
  /**
   * send request to server
   * check if server is available
   * @returns status of response
   */
  public static async updateAccess() {
    let status;

    try {
      const response = await CustomAxios.get();
      status = response.status;
    } catch (error) {
      const err = error as AxiosError;
      status = err.response?.status ?? 400;
    }

    return status;
  }
}
