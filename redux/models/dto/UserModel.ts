import { AxiosError, AxiosResponse } from 'axios';
import { dicStatus } from '../../../constants/dicStatus';
import CustomAxios from '../../../custom-axios/CustomAxios';
import { OTPForgotPasswordRequest, UserForgotPasswordRequest, UserLoginRequest } from '../request';
import { OTPForgotPasswordStatusResponse, UserLoginResponse } from '../response';

export interface UserState {
  user: UserModel;
  forgotPasswordStatus: number;
  updateDate: string;
}

export interface UserModel {
  id: number;
  name?: string;
  image: string;
  email: string;
  phone?: string;
  updateDate?: string;
}
/**
 * actions of user
 */
export class User {

  /**
   * check user is already logged
   * @returns if already logged get user data else return null
   */
  public static async checkLoginFunc() {
    let user: UserModel = {} as UserModel;

    try {
      const response = await CustomAxios.get<UserModel>('user/profile');
      if (dicStatus[response.status] == 'OK') {
        user = response.data;
      }
    } catch (error) {
      user = {} as UserModel;
      const err = error as AxiosError;
      console.log(err.message, 39);
    }

    return user;
  }

  public static async loginFunc(request: UserLoginRequest): Promise<UserModel> {
    let user: UserModel = {} as UserModel;

    try {
      const response: AxiosResponse<UserLoginResponse> =
        await CustomAxios.post<UserLoginResponse>('user/login', request);

      if (dicStatus[response.status] == 'OK') {
        user = response.data;
      }
    } catch (error) {
      const err = error as AxiosError;
      console.log(err.message);
    }

    return user;
  }

  public static async logoutFunc(): Promise<UserModel> {
    let user: UserModel = {} as UserModel;
    try {
      const response: AxiosResponse<UserLoginResponse> =
        await CustomAxios.get('user/logout');

      if (dicStatus[response.status] == 'OK') {
        user = response.data;
      }
    } catch (error) {
      const err = error as AxiosError;
      console.log(err.message);
    }

    return user;
  }

  public static async forgotPasswordFunc(request: UserForgotPasswordRequest): Promise<number> {
    let result = 0;

    try {
      const dataResponse: AxiosResponse<undefined> =
        await CustomAxios.post<undefined>('user/forgot', request);
      result = dataResponse.status;
    } catch (error) {
      const err = error as AxiosError;
      console.log(err.message);
    }

    return result;
  }
  public static async forgotPasswordOTPFunc(request: OTPForgotPasswordRequest): Promise<string> {
    let result = '';

    try {
      const dataResponse: AxiosResponse<OTPForgotPasswordStatusResponse> =
        await CustomAxios.post<OTPForgotPasswordStatusResponse>('user/otp/forgot-password', request);
      result = dataResponse.data.updateDate;
    } catch (error) {
      const err = error as AxiosError;
      console.log(err.message);
    }

    return result;
  }
}




