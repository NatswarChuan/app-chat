export interface UserResponse {
  id: number;
  name: string;
  image: string;
  email: string;
  phone?: string;
  updateDate: string;
}

export interface UserLoginResponse {
  id: number;
  image: string;
  email: string;
}
export interface OTPForgotPasswordStatusResponse {
  updateDate: string;
}

