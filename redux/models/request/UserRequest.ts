export interface UserLoginRequest {
  email: string;
  password: string;
}


export interface UserForgotPasswordRequest {
  email: string;
}


export interface OTPForgotPasswordRequest {
  otp: string;
}
